import { ScullyConfig } from '@scullyio/scully';

export const config: ScullyConfig = {
  projectRoot: './src',
  projectName: 'scully-pipeline-test',
  outDir: './dist/static',
  routes: {},
};
